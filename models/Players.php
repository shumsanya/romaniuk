<?php

namespace app\models;
use Yii;


class Players extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return '{{%players}}';
    }

    public function rules()
    {
        return [
            [['name', 'surname'], 'required'],
            [['name', 'surname', 'position'], 'string'],
            [['command'], 'integer'],
            [['date'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'surname' => 'Surname',
            'date' => 'Date',
            'position' => 'Position',
            'command' => 'Command',
        ];
    }

    public static function newPlayer($x)
    {

        $link = new Players();

        $link->attributes = $x;
        $link->save();

        return true;
    }

    public static function ebitPlayer($id, $x)
    {

        $link = Players::findOne($id);

        $link->attributes = $x;
        $link->save();

        return true;
    }


    public static function getPlayerId($id)
    {
        intval($id);                           // Возвращает целое значение переменной

        $result = self::find()
            ->where(['id' => $id])
            ->one();

        return $result;
    }

    public static function getPlayers($idCommand)
    {
        $result = self::findAll([
            'command' => $idCommand,
        ]);


        return $result;
    }


     public static function deletePlayer($id)
     {
         $x = Players::getPlayerId($id);
         $x->delete();
         return true;
     }


//*******************************************************************************************************************

}