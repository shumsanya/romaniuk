<?php
/**
 * Created by PhpStorm.
 * User: shum
 * Date: 22.03.17
 * Time: 12:50
 */

namespace app\models;


class formTeam extends \yii\base\Model
{
     public $name;
     public $date;

     public function rules(){
         return [
             [['name', 'date'], 'required'],
             [['name', 'date'], 'string'],
         ];
     }

}