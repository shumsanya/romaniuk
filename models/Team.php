<?php

namespace app\models;
use yii\db\ActiveRecord;
use Yii;
use yii\base\Model;

class Team extends ActiveRecord
{
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;

    public static function tableName()
    {
        return '{{team}}';
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string'],
            [['date'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'date' => 'Date',
        ];
    }

    public static function teamNew($x)
    {
        $link = new Team();

        $link->getErrors();
        $link->attributes = $x;
         $link->save();

       /* Yii::$app->db->createCommand('INSERT INTO `team` (`name`) VALUES (:name)', [
            ':name' => 'Qiang',
        ])->execute();*/
        return true;
    }
//*******************************************************************************************************************
    public static function getTeamId($id)
    {
        intval($id);                           // Возвращает целое значение переменной

        $result = self::find()
            ->where(['id' => $id])
            ->one();

        return $result;
    }

    public static function getTeams()
    {
        $result = Yii::$app->db->createCommand('SELECT * FROM team')
            ->queryAll();

        return $result;
    }

    public static function teamDelete($id)
    {
        $x = Team::getTeamId($id);
        $x->delete();
        return true;
    }



    public static function editTeam($id, $x)
    {
        $link = Team::findOne($id);

        $link->attributes = $x;
        $link->save();

        return true;

        return ;
    }

    public function getDateText()
    {
        return date('d.m.Y', $this->day);
    }

    public function setDateText($value)
    {
        $this->day = strtotime($value);
    }

//*******************************************************************************************************************

}