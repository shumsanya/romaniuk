<?php

namespace app\controllers;

use app\models\Team;
use app\models\Players;
use app\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class PlayerController extends Controller
{
    public $enableCsrfValidation = false;

    public function actionPlayers($idCommand)
    {
        $result = Players::getPlayers($idCommand);
        $result1 = Team::getTeamId($idCommand);

        $url = Yii::$app->urlManager->createUrl('player/new');
        //return $this->render('players', ['posts'=>$result, 'posts1'=>$result1]);
        return $this->render('players.twig', ['posts'=>$result, 'posts1'=>$result1, 'url'=>$url]);
    }

    public function actionNew($id)
    {
        if (isset($_POST['ok'])){

            $name= $_POST['name'];
            $surname= $_POST['surname'];
            $date = $_POST['date'];
            $position = $_POST['position'];
            $x = [
                'name' => $name, 'surname' => $surname, 'date' => $date, 'position' => $position, 'command' => $id
            ];

            if (Players::newPlayer($x)) return $this->redirect('index.php?r=player%2Fplayers&idCommand='.$id);
        }

        $result = Team::getTeams();

        //return $this->render('formaPlayers', ['posts'=>$result]);
        return $this->render('formaPlayers.twig', ['posts'=>$result]);
    }


   public function actionEdit($id)
   {
       if (isset($_POST['ok'])){

           $name= $_POST['name'];
           $surname= $_POST['surname'];
           $date = $_POST['date'];
           $position = $_POST['position'];
           $val = Team::getTeams();
           $res = Players::getPlayerId($id);
           $command = $res['command'];

           if (isset($_POST['command'])){

               foreach($val as $com){
                   if ($_POST['command'] == $com['name']){
                       $command = $com['id'];
                   }
               }
               $x = [
                   'name' => $name, 'surname' => $surname, 'date' => $date, 'position' => $position, 'command' => $command
               ];
               if (Players::ebitPlayer($id, $x)) return $this->redirect('index.php?r=player%2Fplayers&idCommand='.$command);
           }else
               $x = [
                   'name' => $name, 'surname' => $surname, 'date' => $date, 'position' => $position, 'command' => $command
               ];
               if (Players::ebitPlayer($id, $x)) return $this->redirect('index.php?r=player%2Fplayers&idCommand='.$command);
       }
       $team = Team::getTeams();
       $result = Players::getPlayerId($id);

       //return $this->render('editPlayers', ['posts'=>$result, 'team'=>$team]);
       return $this->render('editPlayers.twig', ['posts'=>$result, 'team'=>$team]);
   }

    public function actionDel($id)
    {
        $res = Players::getPlayerId($id);
        $command = $res['command'];

        if (isset($_POST['x'])) {
            if (Players::deletePlayer($id))
                return $this->redirect('index.php?r=player%2Fplayers&idCommand='.$command);
        }

        $name = $res['name'];
        return $this->render('delPlayer.twig', ['posts'=>$name]);
    }

}



