<?php

namespace app\controllers;

use app\models\formTeam;
use app\models\Team;

use app\models\Players;
use app\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\Html;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\widgets\ActiveForm;

class SiteController extends Controller
{
    public $enableCsrfValidation = false;

  //  public $layout = 'layout_main.twig';
    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
       $result = User::getTeams();

        $url = Yii::$app->urlManager->createUrl('site/new');

        return $this->render('index.twig', ['posts'=>$result, 'url'=>$url]);
       // return $this->render('index', ['posts'=>$result, 'url'=>$url]);
    }


    public function actionNew()
    {
        if (isset($_POST['ok'])){
            $name= $_POST['name'];
            $date = $_POST['date'];
            $x = [
            'name' => $name,
            'date' => $date
            ];

            if (Team::teamNew($x)) return $this->redirect('index.php');
        }

        //return $this->render('formaTeam', ['form' => $form]);
        return $this->render('formaTeam.twig');
    }


    public function actionEdit($id)
    {
        $result = Team::getTeamId($id);

        if (isset($_POST['ok'])){
            $name= $_POST['name'];
            $date = $_POST['date'];
            $x = [
                'name' => $name,
                'date' => $date
            ];

            if (Team::editTeam($id, $x)) return $this->redirect('index.php');
        }

        //return $this->render('editTeam', ['posts'=>$result]);
        return $this->render('editTeam.twig', ['posts'=>$result]);
    }


    public function actionDel($id)
    {
        $result = Team::getTeamId($id);

        if (isset($_POST['x'])&& Team::teamDelete($id)) {

           return $this->redirect('index.php');
        }
        $y = $result['name'];
        return $this->render('delTeam.twig', ['posts'=>$y]); // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
       // return $this->render('delTeam', ['posts'=>$y]); // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    }

}
