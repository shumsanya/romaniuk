<?php

$this->title = 'Гравці';
/**
 * Created by PhpStorm.
 * User: shum
 * Date: 15.03.17
 * Time: 11:08
 */
?>
<button class="button" onclick="location.href='<?=Yii::$app->urlManager->createUrl('player/new')?>'"> Добавити гравця</button>
<p><h3>Команда - <?php echo $posts1['name']?></h3></p>
<div class="datagrid">
    <table>
        <thead>
        <tr><th>id</th><th>Імя</th><th>Прізвище</th><th>Дата народження</th><th>Позиція на полі</th><th>редагувати</th><th>видалити</th></tr>
        </thead>
        <tfoot>
        <tr><td colspan="7">
                <div id="paging">
                    <ul>
                        <li><a href="#"><span>Previous</span></a></li>
                        <li><a href="#" class="active"><span>1</span></a></li>
                        <li><a href="#"><span>2</span></a></li>
                        <li><a href="#"><span>3</span></a></li>
                        <li><a href="#"><span>4</span></a></li>
                        <li><a href="#"><span>5</span></a></li>
                        <li><a href="#"><span>Next</span></a></li>
                    </ul>
                </div>
        </tr>
        </tfoot>
        <tbody>
        <?php $num=1; foreach($posts as $x):?>
        <?php $num++; if ($num%2==0):?>
        <tr>
            <td><?php echo $x['id']?></td>
            <td><?php echo $x['name']?></td>
            <td><?php echo $x['surname']?></td>
            <td><?php echo $x['date']?></td>
            <td><?php echo $x['position']?></td>
            <td><a href="<?=Yii::$app->urlManager->createUrl(['player/edit', 'id' => $x['id']])?>">Редагувати</a></td>
            <td><a href="<?=Yii::$app->urlManager->createUrl(['player/del', 'id' => $x['id']])?>">Видалити</a></td>
        </tr>
        <?php else: ?>
        <tr class="alt">
            <td><?php echo $x['id']?></td>
            <td><?php echo $x['name']?></td>
            <td><?php echo $x['surname']?></td>
            <td><?php echo $x['date']?></td>
            <td><?php echo $x['position']?></td>
            <td><a href="<?=Yii::$app->urlManager->createUrl(['player/edit', 'id' => $x['id']])?>">Редагувати</a></td>
            <td><a href="<?=Yii::$app->urlManager->createUrl(['player/del', 'id' => $x['id']])?>">Видалити</a></td>
        </tr>
            <?php endif;?>
        <?php endforeach ?>
        </tbody>
    </table>
</div>

<br><br>
<p><h4><- на сторінку <a href="<?=Yii::$app->urlManager->createUrl('/')?>"> Команди </a> </h4></p>

