<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;

/* @var $this yii\web\View */

$this->title = 'Команди';
?>

<form action="/myTest/web/index.php?r=site%2Fnew" method="post">
    <input id="form-token" type="hidden" name="<?=Yii::$app->request->csrfParam?>"
           value="<?=Yii::$app->request->csrfToken?>">
    <input class="button" type="submit" name="x" value="ok!">
  <!--  <button class="button" type="submit" name="x"> Добавити команду</button>-->
</form>

<p><a class="btn btn-lg btn-success" href="<?php $url?>">Добавити команду</a></p>

<p><h3> Команди </h3></p>
<div class="datagrid">
    <table>
        <thead>
        <tr><th>id</th><th>Назва команди</th><th>рік заснування</th><th>редагувати</th><th>видалити</th></tr>
        </thead>
        <tfoot>
        <tr><td colspan="5">
                <div id="paging">
                    <ul>
                        <li><a href="#"><span>Previous</span></a></li>
                        <li><a href="#" class="active"><span>1</span></a></li>
                        <li><a href="#"><span>2</span></a></li>
                        <li><a href="#"><span>3</span></a></li>
                        <li><a href="#"><span>4</span></a></li>
                        <li><a href="#"><span>5</span></a></li>
                        <li><a href="#"><span>Next</span></a></li>
                    </ul>
                </div>
        </tr>
        </tfoot>
        <tbody>

        <?php $num=1; foreach($posts as $x):?>
            <?php $num++; if ($num%2==0):?>
            <tr>
                <td><?php echo $x['id']?></td>
                <td><a href="<?=Yii::$app->urlManager->createUrl(['player/players', 'idCommand' => $x['id']])?>"><?php echo $x['name']?></a></td>
                <td><?php echo $x['date']?></td>
                <td><a href="<?=Yii::$app->urlManager->createUrl(['site/edit', 'id' => $x['id']])?>">Редагувати</a></td>
                <td><a href="<?=Yii::$app->urlManager->createUrl(['site/del', 'id' => $x['id']])?>">Видалити</a></td>
            </tr>
            <?php else: ?>
            <tr class="alt">
                <td><?php echo $x['id']?></td>
                <td><a href="<?=Yii::$app->urlManager->createUrl(['player/players', 'idCommand'=> $x['id']])?>"><?php echo $x['name']?></a></td>
                <td><?php echo $x['date']?></td>
                <td><a href="<?=Yii::$app->urlManager->createUrl(['site/edit', 'id' => $x['id']])?>">Редагувати</a></td>
                <td><a href="<?=Yii::$app->urlManager->createUrl(['site/del', 'id' => $x['id']])?>">Видалити</a></td>
            </tr>
        <?php endif;?>
        <?php endforeach ?>

        <?php print_r($posts) ?>
        </tbody>
    </table>
</div>
<br><br>






